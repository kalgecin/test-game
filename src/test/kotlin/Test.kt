import com.example.testgame.loadNeatPopulation
import com.example.testgame.round
import org.junit.Assert
import org.junit.Test

//fun main() {
//    val s = loadNeatPopulation("savedNEAT.json").map { it.nodes.size - it.input - it.output }
//    println("${s.max()} ${s.average()} ${s.min()}")
//}

class Test {
    @Test
    fun testPow() {
        val num = 0.123456
        val expected = 0.12
        val actual = num.round(2)
        Assert.assertEquals(expected, actual, 0.001)
    }
}
