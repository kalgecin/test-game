package com.example.testgame.microservice

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.example.testgame.GameUI
import com.example.testgame.game.AiGame
import com.kalgecin.neataptic4k.Network
import getLogger
import io.micronaut.context.annotation.Requires
import io.micronaut.http.annotation.Body
import io.micronaut.rabbitmq.annotation.Queue
import io.micronaut.rabbitmq.annotation.RabbitListener
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromByteArray
import kotlinx.serialization.encodeToByteArray
import kotlinx.serialization.protobuf.ProtoBuf
import java.nio.file.Files
import java.nio.file.Paths
import javax.annotation.PostConstruct
import kotlin.random.Random

@ExperimentalSerializationApi
@RabbitListener
@Requires(env = ["local"])
class ApplicationListener {
    private val log = getLogger(javaClass)
    private val filename = "bestNetwork.json"
    var bestNetwork: Network?
    private val config: LwjglApplicationConfiguration
    private val game: AiGame
    private var randomSeed = Random.nextLong()

    init {
        config = LwjglApplicationConfiguration().apply {
            width = 1280
            height = 720
        vSyncEnabled = true
//        backgroundFPS = 60
//        foregroundFPS = 60
        }
        val bestNetworkPath = Paths.get(filename)
        bestNetwork = if (Files.exists(bestNetworkPath)) {
            val networkJsonString = Files.readAllBytes(bestNetworkPath)
            val copyObject = ProtoBuf.decodeFromByteArray<Network.CopyObject>(networkJsonString)
            Network.deserialize(copyObject)
        } else null
        game = AiGame(config.width, config.height, bestNetwork, Random(randomSeed))
    }

    @Queue("\${app.config.host}")
    fun receive(@Body data: ByteArray) {
        val value = ProtoBuf.decodeFromByteArray<Network.CopyObject>(data)
        val network = Network.deserialize(value)
        bestNetwork = network
        log.info("Received new Network ${network.nodes.size - network.input - network.output}:${network.connections.size}")
        if (game.network == null) {
            game.network = network
        }
        saveBest(value)
    }

    fun saveBest(copyObject: Network.CopyObject) {
        val neatJson = ProtoBuf.encodeToByteArray(copyObject)
        Files.write(Paths.get(filename), neatJson)
    }

    @PostConstruct
    fun postConstruct() {

        LwjglApplication(GameUI(game), config)
        GlobalScope.launch {
            while (true) {
                if (!game.playerAlive) {
                    log.info(game.score.toString())
                    if(Random.nextBoolean()) randomSeed = Random.nextLong()
                    game.network = bestNetwork
                    game.network?.clear()
                    game.reset()
                    game.random = Random(randomSeed)
                }
                delay(5000)
            }
        }
    }
}

