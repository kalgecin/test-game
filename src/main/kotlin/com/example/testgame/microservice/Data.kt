package com.example.testgame.microservice

import kotlinx.serialization.Serializable

@Serializable
data class DoneNetwork(
    val score: Double,
    val network: ByteArray
)

@Serializable
data class Job(
    val seed: Long,
    val network: ByteArray
)

@Serializable
data class OffspringRequest(
    val parent1: ByteArray,
    val parent2: ByteArray,
    val mutationRate: Double,
    val mutationAmount: Int
)
