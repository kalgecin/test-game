package com.example.testgame.microservice

import com.example.testgame.data.ApplicationProperties
import com.rabbitmq.client.Channel
import getLogger
import io.micronaut.context.ApplicationContext
import io.micronaut.rabbitmq.connect.ChannelInitializer
import java.io.IOException
import javax.inject.Singleton

@Singleton
open class ChannelPoolListener(
    private val applicationProperties: ApplicationProperties,
    private val context: ApplicationContext
) : ChannelInitializer() {
    lateinit var channel: Channel
    private val log = getLogger(javaClass)

    @Throws(IOException::class)
    override fun initialize(channel: Channel) {
        channel.queueDeclare("newNetworks", true, false, false, null)
        channel.queueDeclare("doneNetworks", true, false, false, null)
        channel.queueDeclare("offspringRequest", true, false, false, null)
        channel.queueDeclare("offspringResponse", true, false, false, null)
        this.channel = channel
        if (context.environment.activeNames.contains("local")) {
            channel.queueDeclare(applicationProperties.host, false, false, true, null)
            channel.queueBind(applicationProperties.host, "amq.fanout", "")
            log.info("Bound ${applicationProperties.host} to bestNetwork")
        }
        log.info("initialized pool")
    }
}
