package com.example.testgame.microservice

import com.example.testgame.data.ApplicationProperties
import com.example.testgame.loadNeatPopulation
import com.example.testgame.printStats
import com.kalgecin.neataptic4k.NEAT
import com.kalgecin.neataptic4k.Network
import getLogger
import io.micronaut.context.annotation.Requires
import io.micronaut.http.annotation.Body
import io.micronaut.rabbitmq.annotation.Binding
import io.micronaut.rabbitmq.annotation.Queue
import io.micronaut.rabbitmq.annotation.RabbitClient
import io.micronaut.rabbitmq.annotation.RabbitListener
import io.micronaut.scheduling.annotation.Scheduled
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromByteArray
import kotlinx.serialization.encodeToByteArray
import kotlinx.serialization.protobuf.ProtoBuf
import loadNeat
import saveNeat
import java.lang.Thread.sleep
import javax.annotation.PostConstruct
import javax.inject.Singleton
import kotlin.math.floor
import kotlin.math.pow
import kotlin.random.Random

@ExperimentalSerializationApi
@Singleton
@Requires(env = ["master"])
class MasterService(
    private val applicationProperties: ApplicationProperties,
    private val masterProducer: MasterProducer,
    private val bestProducer: BestProducer,
    private val channelPoolListener: ChannelPoolListener
) {
    private val log = getLogger(javaClass)
    private val neat: NEAT
    private var lastReceived: Long = 0
    private val neatFilename = "savedNEAT.json"

    init {
        cleanQueues()
        neat = loadNeat(applicationProperties.neatFilename, applicationProperties.poolSize)
        log.info("Loaded master service: ${applicationProperties.poolSize}")
    }

    @PostConstruct
    fun postConstruct() {
        log.info("Sending new networks: ${applicationProperties.poolSize}")
        cleanQueues()
        sleep(5000)
        distributeWork()
    }

    private fun cleanQueues() {
        channelPoolListener.channel.queuePurge("newNetworks")
        channelPoolListener.channel.queuePurge("doneNetworks")
        channelPoolListener.channel.queuePurge("offspringRequest")
        channelPoolListener.channel.queuePurge("offspringResponse")
    }

    @Scheduled(initialDelay = "20m", fixedRate = "10m")
    fun abc() {
        val intervalMins = (System.currentTimeMillis() - lastReceived) / (1000 * 60)

        if (intervalMins <= 10) return
        log.warn("No new networks received in the last ${intervalMins}m")
        if (neat.population.size <= 0) {
            neat.population = loadNeatPopulation(neatFilename).toMutableList()
            log.warn("Loaded neat from saved file")
        } else {
            log.warn("Forcing evolution with ${neat.population.size}")
        }
        neat.printStats(log)
        mutateAndSend()
    }

    fun receiveNetwork(network: Network) {
        neat.population.add(network)
        lastReceived = System.currentTimeMillis()
        if (neat.population.size >= applicationProperties.poolSize) {
            neat.printStats(log)
            mutateAndSend()
        }
    }

    private fun mutateAndSend() {
        neat.sort()
        if (!neat.population[0].score.isNaN() && neat.population[0].score != neat.population.last().score) {
            broadcastBestNetwork()
            saveNeat(neat)
            distributedEvolution()
        } else {
            println("${!neat.population[0].score.isNaN()} ${neat.population[0].score} != ${neat.population.last().score}")
            distributeWork()
        }
    }

    private fun distributedEvolution() {
        neat.sort()
        val elitists = neat.population.take(neat.options.elitism)
        neat.population = elitists.toMutableList()
        (0 until neat.options.populationSize - neat.options.elitism).forEach { _ ->
            val parent1 = ProtoBuf.encodeToByteArray(getParent().serialize())
            val parent2 = ProtoBuf.encodeToByteArray(getParent().serialize())
            val request = OffspringRequest(parent1, parent2, neat.options.mutationRate, Random.nextInt(1, 10))
            masterProducer.sendOffspringRequest(ProtoBuf.encodeToByteArray(request))
        }
        neat.generation++
    }

    private fun getParent(): Network {
        return neat.population[floor(
            Random.nextDouble().pow(neat.options.selection.power) * neat.population.size
        ).toInt()]
    }

    private fun broadcastBestNetwork() {
        val bestNetwork = neat.population.maxByOrNull { it.score }!!
        val bestSer = ProtoBuf.encodeToByteArray(bestNetwork.serialize())
        bestProducer.sendBestNetwork(bestSer)
    }

    fun receiveOffspring(network: Network) {
        neat.population.add(network)
        if (neat.population.size == neat.options.populationSize) {
            distributeWork()
        }
    }

    private fun distributeWork() {
        val seed = Random.nextLong()
        val population = neat.population.toList()
        neat.population.clear()
        population.forEach {
            val n = ProtoBuf.encodeToByteArray(it.serialize())
            val job = Job(seed, n)
            masterProducer.sendNewNetwork(ProtoBuf.encodeToByteArray(job))
        }
    }
}

@RabbitClient
@Requires(env = ["master"])
interface MasterProducer {
    @Binding("newNetworks")
    fun sendNewNetwork(job: ByteArray)

    @Binding("offspringRequest")
    fun sendOffspringRequest(request: ByteArray)
}

@RabbitClient("amq.fanout")
@Requires(env = ["master"])
interface BestProducer {
    fun sendBestNetwork(network: ByteArray)
}

@ExperimentalSerializationApi
@RabbitListener
@Requires(env = ["master"])
class MasterListener(private val service: MasterService) {

    @Queue("doneNetworks")
    fun receive(@Body value: ByteArray) {
        val doneNetwork = ProtoBuf.decodeFromByteArray(DoneNetwork.serializer(), value)
        val network = Network.deserialize(ProtoBuf.decodeFromByteArray(doneNetwork.network))
        network.score = doneNetwork.score
        service.receiveNetwork(network)
    }

    @Queue("offspringResponse")
    fun receiveOffspring(value: ByteArray) {
        val network = Network.deserialize(ProtoBuf.decodeFromByteArray(value))
        service.receiveOffspring(network)
    }
}
