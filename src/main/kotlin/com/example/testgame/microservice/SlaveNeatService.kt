package com.example.testgame.microservice

import com.example.testgame.data.ApplicationProperties
import com.example.testgame.game.AiGame
import com.example.testgame.round
import com.kalgecin.neataptic4k.EvolveOptions
import com.kalgecin.neataptic4k.MutationType
import com.kalgecin.neataptic4k.NEAT
import com.kalgecin.neataptic4k.Network
import getLogger
import io.micronaut.context.annotation.Requires
import io.micronaut.http.annotation.Body
import io.micronaut.rabbitmq.annotation.Binding
import io.micronaut.rabbitmq.annotation.Queue
import io.micronaut.rabbitmq.annotation.RabbitClient
import io.micronaut.rabbitmq.annotation.RabbitListener
import io.reactivex.Completable
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import kotlinx.coroutines.coroutineScope
import kotlinx.coroutines.runBlocking
import kotlinx.serialization.ExperimentalSerializationApi
import kotlinx.serialization.decodeFromByteArray
import kotlinx.serialization.encodeToByteArray
import kotlinx.serialization.protobuf.ProtoBuf
import javax.inject.Singleton
import kotlin.random.Random
import kotlin.system.measureTimeMillis

@ExperimentalSerializationApi
@Singleton
@Requires(env = ["slave"])
class SlaveSingleService(
    private val applicationProperties: ApplicationProperties,
    private val slaveProducer: SlaveProducer
) {
    private val log = getLogger(javaClass)

    fun receiveNetwork(network: Network, seed: Long) {
        val game =
            AiGame(applicationProperties.width, applicationProperties.height, network, Random(seed))
        val time = measureTimeMillis {
            while (game.playerAlive) {
                game.tick()
            }
        } / 1000.0
        network.score = game.score.toDouble()
        log.info("${time.round(2)}s ${network.score}")
        val serializedNetwork = ProtoBuf.encodeToByteArray(network.serialize())
        val doneNetwork = DoneNetwork(network.score, serializedNetwork)
        slaveProducer.sendNetwork(ProtoBuf.encodeToByteArray(doneNetwork)).subscribe { }
    }

    fun generateOffspring(request: OffspringRequest) {
        val p1 = Network.deserialize(ProtoBuf.decodeFromByteArray(request.parent1))
        val p2 = Network.deserialize(ProtoBuf.decodeFromByteArray(request.parent1))
        val newNetwork = Network.crossover(p1, p2, false)
        if (Random.nextDouble() <= request.mutationRate) {
            repeat(request.mutationAmount) { newNetwork.mutate(MutationType.ALL.random()) }
        }
        val newNetworkStr = ProtoBuf.encodeToByteArray(newNetwork.serialize())
        slaveProducer.sendOffspring(newNetworkStr).subscribe { }
    }
}

@ExperimentalSerializationApi
@Singleton
@Requires(env = ["slave"])
class SlaveNeatService(
    private val applicationProperties: ApplicationProperties,
    private val slaveProducer: SlaveProducer
) {
    private val log = getLogger(javaClass)

    fun receiveNetwork(network: Network, seed: Long) {
        val mutationTypes = MutationType.ALL.filter { Random.nextBoolean() }.toMutableList()
        if (mutationTypes.isEmpty()) mutationTypes.addAll(MutationType.FFW)
        val options = EvolveOptions(
            populationSize = applicationProperties.poolSize,
            network = network,
            elitism = applicationProperties.poolSize / 10,
            isClear = true,
            mutationRate = Random.nextDouble(0.01, 0.5).round(2),
            mutationTypes = mutationTypes.toTypedArray(),
            mutationAmount = Random.nextInt(1, 10)
        )
        val neat = NEAT(
            input = network.input,
            output = network.output,
            options = options,
            fitnesFunction = null
        )
        neat.mutate()
        neat.population[0] = network
        val time = measureTimeMillis {
            runBlocking {
                runNeat(neat, seed)
            }
        } / 1000.0
        val bestNetwork = neat.population.maxByOrNull { it.score }!!
        val scores = neat.population.map { it.score }.sortedDescending()
        val max = scores.maxOrNull()
        val min = scores.minOrNull()
        val avg = scores.average()
        val topAvg = scores.take(10).average()
        log.info("${time}s $max $topAvg $avg $min (mutations: ${options.mutationAmount}, ${options.mutationRate}, ${mutationTypes.size})")
        val serializedNetwork = ProtoBuf.encodeToByteArray(bestNetwork.serialize())
        val doneNetwork = DoneNetwork(bestNetwork.score, serializedNetwork)
        slaveProducer.sendNetwork(ProtoBuf.encodeToByteArray(doneNetwork))
    }

    private suspend fun runNeat(neat: NEAT, randomSeed: Long) = coroutineScope {
        neat.population.map { network ->
            GlobalScope.async {
                val game =
                    AiGame(applicationProperties.width, applicationProperties.height, network, Random(randomSeed))
                while (game.playerAlive) {
                    game.tick()
                }
                network.score = game.score.toDouble()
            }
        }.forEach { it.await() }
    }
}

@RabbitClient
@Requires(env = ["slave"])
interface SlaveProducer {
    @Binding("doneNetworks")
    fun sendNetwork(network: ByteArray): Completable

    @Binding("offspringResponse")
    fun sendOffspring(network: ByteArray): Completable
}

@RabbitListener
@Requires(env = ["slave"])
@ExperimentalSerializationApi
class SlaveListener(private val service: SlaveSingleService) {

    @Queue(value = "newNetworks", prefetch = 10)
    fun receive(@Body data: ByteArray) {
        val value = ProtoBuf.decodeFromByteArray<Job>(data)
        val network = Network.deserialize(ProtoBuf.decodeFromByteArray(value.network))
        service.receiveNetwork(network, value.seed)
    }

    @Queue("offspringRequest", prefetch = 10)
    fun receiveEvolutionRequest(data: ByteArray) {
        service.generateOffspring(ProtoBuf.decodeFromByteArray(data))
    }
}
