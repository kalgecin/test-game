package com.example.testgame

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.example.testgame.game.AiGame
import com.example.testgame.game.Game
import com.kalgecin.neataptic4k.Network
import ktx.app.KtxApplicationAdapter
import ktx.app.clearScreen
import ktx.graphics.use

class GameUI(private val game: Game) : KtxApplicationAdapter {
    private lateinit var renderer: ShapeRenderer
    private lateinit var font: BitmapFont
    private lateinit var batch: SpriteBatch

    override fun create() {
        renderer = ShapeRenderer()
        font = BitmapFont()
        batch = SpriteBatch()
    }

    override fun render() {
        game.tick()
        if (!game.playerAlive && game.getKeyPressed() == Input.Keys.Y) game.reset()
        renderer.use(ShapeRenderer.ShapeType.Filled) {
            Gdx.graphics.setTitle("${game.score}, ${game.roidsKilled} " + Gdx.graphics.framesPerSecond)
        }
        draw()
    }

    private fun draw() {
        clearScreen(0f, 0f, 0f, 0f)
        val player = game.player
        renderer.use(ShapeRenderer.ShapeType.Filled) {
            val p = player.position
            renderer.color = Color.RED
            renderer.rect(
                p.x - player.size / 2,
                p.y - player.size / 2,
                player.size / 2,
                player.size / 2,
                player.size,
                player.size,
                1f,
                1f,
                player.direction
            )
            val w = p.translate(player.size, player.direction)
            renderer.circle(w.x, w.y, 2f)
        }
        renderer.use(ShapeRenderer.ShapeType.Line) {
            renderer.circle(player.position.x, player.position.y, player.size)
        }
        renderer.use(ShapeRenderer.ShapeType.Filled) {
            renderer.color = Color.BROWN
            game.bodies.toList().forEach {
                renderer.circle(it.position.x, it.position.y, it.size)
            }
        }
        renderer.use(ShapeRenderer.ShapeType.Line) {
            renderer.color = Color.GREEN
            game.closestBodies().forEach {
                renderer.circle(it.position.x, it.position.y, it.size)
            }
        }
        batch.begin()
        var pos = 12f
        if(game is AiGame)
            font.draw(batch, "Hidden Size: ${game.network?.hiddenSize()}:${game.network?.connections?.size}", 0f, pos); pos += 15
        font.draw(batch, "FPS: ${Gdx.graphics.framesPerSecond}", 0f, pos); pos += 15
        font.draw(batch, "Asteroid Destroyed: ${game.roidsKilled}", 0f, pos); pos += 15
        font.draw(batch, "Score: ${game.score}", 0f, pos); pos += 15
        batch.end()

    }
    fun Network.hiddenSize(): Int = nodes.size - input - output
}
