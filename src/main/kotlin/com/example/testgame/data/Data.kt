package com.example.testgame.data

import kotlin.math.PI
import kotlin.math.cos
import kotlin.math.sin

data class Body(
    val position: Position,
    var direction: Float = 0f,
    var speed: Double = 0.0,
    val size: Float = 20f
) {
    fun move() {
        val rad = direction.toRadians() * -1
        position.x += (speed * sin(rad)).toFloat()
        position.y += (speed * cos(rad)).toFloat()
    }
}

data class Position(
    var x: Float,
    var y: Float
) {
    fun translate(distance: Float, angle: Float) : Position {
        val r = angle.toRadians() * -1
        return copy(
            x = (x + (distance * sin(r))).toFloat(),
            y = (y + (distance * cos(r))).toFloat()
        )
    }
}

private fun Float.toRadians(): Double {
    return this * PI / 180
}
