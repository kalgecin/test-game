package com.example.testgame.data

import io.micronaut.context.annotation.ConfigurationProperties

@ConfigurationProperties("app.config")
class ApplicationProperties {
    var poolSize: Int = 100
    var neatFilename: String = "savedNEAT.json"
    var width: Int = 1024
    var height: Int = 720
    var host:String = ""
    var prefetchCount: Int = 1
}
