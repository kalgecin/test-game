package com.example.testgame.networkview

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.example.testgame.data.Position
import com.example.testgame.game.AiGame
import com.kalgecin.neataptic4k.Network
import com.kalgecin.neataptic4k.Node
import com.kalgecin.neataptic4k.NodeTypeEnum
import ktx.app.KtxApplicationAdapter
import ktx.app.clearScreen
import ktx.graphics.use
import kotlin.random.Random

class NetworkViewUI(val game: AiGame) : KtxApplicationAdapter {
    private lateinit var renderer: ShapeRenderer
    private lateinit var font: BitmapFont
    private lateinit var batch: SpriteBatch
    private var networkId = game.network?.id ?: -1
    private val nodeMap = mutableMapOf<Node, Position>()

    init {
        game.network?.let {
            nodeMap.clear()
            mapNetwork(it)
        }
    }

    override fun create() {
        renderer = ShapeRenderer()
        font = BitmapFont()
        batch = SpriteBatch()
    }

    override fun render() {
        clearScreen(0f, 0f, 0f, 0f)
        val network = game.network ?: return
        if (network.id != networkId) {
            networkId = network.id
            nodeMap.clear()
            mapNetwork(network)
        }
        renderer.use(ShapeRenderer.ShapeType.Filled) {
            renderer.color = Color.RED
            val circleSize = (game.height / network.input.toDouble()) / 2
            network.nodes.take(network.input).forEach { node ->
                val pos = nodeMap[node]!!
                renderer.circle(pos.x, pos.y, circleSize.toFloat())
            }
        }
        renderer.use(ShapeRenderer.ShapeType.Filled) {
            renderer.color = Color.GREEN
            val circleSize = (game.height / network.output.toDouble()) / 2
            network.nodes.takeLast(network.output).forEach { node ->
                val pos = nodeMap[node]!!
                renderer.circle(pos.x, pos.y, circleSize.toFloat())
            }
        }
        renderer.use(ShapeRenderer.ShapeType.Filled) {
            renderer.color = Color.BLUE
            network.nodes
                .drop(network.input)
                .take(network.nodes.size - network.input - network.output)
                .forEach {
                    val pos = nodeMap[it]!!
                    renderer.circle(pos.x, pos.y, 20f)
                }
        }
        network.nodes.forEach {
            val pos = nodeMap[it]!!
            batch.begin()
            val bias = (it.bias * 100).toInt() / 100.0
            font.draw(batch, bias.toString(), pos.x - 15, pos.y + 6)
            batch.end()
        }
        nodeMap.forEach { (node, fromPos) ->
            node.connections.outputs
                .filter { it.weight > 0 }
                .mapNotNull { nodeMap[it.to] }
                .forEach { toPos ->
                    renderer.use(ShapeRenderer.ShapeType.Filled) {
                        renderer.color = Color.FOREST
                        renderer.line(fromPos.x, fromPos.y, toPos.x, toPos.y)
                        //TODO: write weight
                    }
                }
        }
    }

    fun mapNetwork(network: Network) {
        val inputCircleSize = (game.height / network.input.toDouble()) / 2
        network.nodes.filter { it.type == NodeTypeEnum.INPUT }.forEachIndexed { index, node ->
            val pos = Position(
                x = 0f,
                y = ((index * inputCircleSize * 2) + inputCircleSize).toFloat()
            )
            nodeMap[node] = pos
        }

        val outputCircleSize = (game.height / network.output.toDouble()) / 2
        network.nodes.filter { it.type == NodeTypeEnum.OUTPUT }.forEachIndexed { index, node ->
            val pos = Position(
                x = game.width.toFloat(),
                y = ((index * outputCircleSize * 2) + outputCircleSize).toFloat()
            )
            nodeMap[node] = pos
        }

        network.nodes.filter { it.type != NodeTypeEnum.INPUT && it.type != NodeTypeEnum.OUTPUT }.forEach {
            val pos = Position(
                Random.nextDouble(inputCircleSize + 20, game.width - outputCircleSize - 20).toFloat(),
                Random.nextDouble(20.0, game.height.toDouble() - 20).toFloat()
            ) //TODO: calc pos
            nodeMap[it] = pos
            //TODO: write bias
        }
    }
}
