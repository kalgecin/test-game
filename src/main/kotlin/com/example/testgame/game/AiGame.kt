package com.example.testgame.game

import com.badlogic.gdx.Input
import com.example.testgame.data.Body
import com.kalgecin.neataptic4k.Network
import kotlin.random.Random

open class AiGame(
    val width: Int,
    val height: Int,
    var network: Network? = null,
    random: Random = Random(Random.nextLong())
) : Game(width, height, random) {

    override fun getKeyPressed(): Int? {
        val network = network ?: return null
        val inputs = getInputs()
        val output = network.noTraceActivate(inputs.toDoubleArray())
        val index = output.withIndex().filter { it.index > 2 }.maxBy { (_, v) -> v }?.index
        return when (index) {
            0 -> Input.Keys.A
            1 -> Input.Keys.D
            2 -> Input.Keys.W
            3 -> Input.Keys.S
            4 -> Input.Keys.Q
            5 -> Input.Keys.E
            6 -> Input.Keys.SPACE
            else -> null
        }
    }

    private fun getInputs(): MutableList<Double> {
        val network = network ?: return mutableListOf()
        val inputs = mutableListOf<Double>()
        inputs.add(score.toDouble())
        inputs.addAll(player.toInputs())
        closestBodies().forEach { inputs.addAll(it.toInputs()) }
        if (inputs.size < network.input) {
            repeat(network.input - inputs.size) { inputs.add(0.0) }
        }
        return inputs
    }
}

fun Body.toInputs(): Array<Double> {
    return arrayOf(
        position.x.toDouble(),
        position.y.toDouble(),
        direction.toDouble(),
        speed,
        size.toDouble()

    )
}
