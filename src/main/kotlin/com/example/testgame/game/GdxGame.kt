package com.example.testgame.game

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Input

class GdxGame(width: Int, height: Int): Game(width, height) {
    private var lastBtnPressTime = 0L
    override fun getKeyPressed(): Int? {
        val curTime = System.currentTimeMillis()
        if (curTime - lastBtnPressTime < 1000) {
            return null
        }
        lastBtnPressTime = curTime

        return  when {
            Gdx.input.isKeyPressed(Input.Keys.A) -> Input.Keys.A
            Gdx.input.isKeyPressed(Input.Keys.D) -> Input.Keys.D
            Gdx.input.isKeyPressed(Input.Keys.W) -> Input.Keys.W
            Gdx.input.isKeyPressed(Input.Keys.S) -> Input.Keys.S
            Gdx.input.isKeyPressed(Input.Keys.Q) -> Input.Keys.Q
            Gdx.input.isKeyPressed(Input.Keys.E) -> Input.Keys.E
            Gdx.input.isKeyPressed(Input.Keys.Y) -> Input.Keys.Y
            Gdx.input.isKeyPressed(Input.Keys.SPACE) -> Input.Keys.SPACE
            else -> null
        }
    }
}
