package com.example.testgame.game

import com.badlogic.gdx.Input
import com.example.testgame.data.Body
import com.example.testgame.data.Position
import kotlin.math.pow
import kotlin.math.sqrt
import kotlin.random.Random

abstract class Game(private val width: Int, private val height: Int, var random: Random = Random(Random.nextLong())) {

    val bulletSize = 1f
    private val roidSizeLimit = 2.5
    private var moveDelta = 10f
    private var ticks = 0L


    var playerAlive = true
    var score = 0L
    var roidsKilled = 0
    var player = Body(Position((width / 2).toFloat(), (height / 2).toFloat()))
    val bodies = mutableListOf<Body>()

    abstract fun getKeyPressed(): Int?

    @Synchronized
    open fun tick() {
        if (!playerAlive) return
        ticks++
        handleInput()
        player.move()
        if (random.nextDouble() > 0.99) {
            val isX = random.nextBoolean()
            val body = Body(
                position = Position(
                    if (isX) random.nextFloat() * width else 0f,
                    if (!isX) random.nextFloat() * height else 0f
                ),
                direction = random.nextFloat() * 360,
                speed = random.nextDouble() * 5,
                size = 10 + (random.nextFloat() * 20)
            )
            bodies.add(body)
        }
        bodies.forEach {
            it.move()
            it.checkBounds()
        }
        playerAlive = playerAlive()
        processCollisions()
    }

    fun reset() {
        playerAlive = true
        player = Body(Position((width / 2).toFloat(), (height / 2).toFloat()))
        roidsKilled = 0
        bodies.clear()
        ticks = 0
        score = 0
    }

    fun closestBodies(): List<Body> {
        return bodies.toList()
            .sortedByDescending { -distance(it.position, player.position) }
            .take(9)
    }

    private fun handleInput() {
        when (getKeyPressed()) {
            Input.Keys.W -> player.speed += 0.05
            Input.Keys.S -> player.speed -= 0.05
            Input.Keys.SPACE -> fire()
            Input.Keys.Q -> {
                player.direction += moveDelta / 2
                player.direction %= 360
            }
            Input.Keys.E -> {
                player.direction -= moveDelta / 2
                player.direction %= 360
            }
        }
        player.checkBounds()
    }

    private fun fire() {
        val numBullets = bodies.count { it.size == bulletSize }
        if (score < numBullets && numBullets > 0) return
        val bullet = Body(
            position = player.position.translate(player.size, player.direction),
            speed = 5.0,
            direction = player.direction,
            size = bulletSize
        )
        bodies.add(bullet)
        score -= numBullets
    }

    private fun processCollisions() {
        val toRemove = mutableListOf<Body>()
        val toAdd = mutableListOf<Body>()
        bodies.forEachIndexed { index, body ->
            for (i in index + 1 until bodies.size) {
                val body2 = bodies[i]
                val distance = distance(body.position, body2.position)
                val minDistance = body.size + body2.size
                if (distance <= minDistance) {
                    if (eitherIsBullet(body, body2) && bothNotBullet(body, body2)) {
                        roidsKilled++
                        score += 5
                    }
                    toRemove.add(body2)
                    toRemove.add(body)
                    toAdd.addAll(body.split())
                    toAdd.addAll(body2.split())
                }
            }
        }
        bodies.removeAll(toRemove)
        toAdd.sortByDescending { it.size }
        bodies.addAll(toAdd.take(100))
    }

    private fun eitherIsBullet(body: Body, body2: Body): Boolean {
        return body.size == bulletSize || body2.size == bulletSize
    }

    private fun bothNotBullet(body: Body, body2: Body): Boolean {
        return body.size != bulletSize || body2.size != bulletSize
    }

    private fun Body.split(): MutableList<Body> {
        if (size < roidSizeLimit * 1.5) return mutableListOf()
        val r = random.nextDouble(0.4, 0.6)
        val newSize = (size * r).toFloat()
        val newSize2 = size - newSize
        val toAdd = mutableListOf<Body>()
        val angle = 90.0
        if (newSize > roidSizeLimit) {
            toAdd.add(
                copy(
                    size = newSize,
                    position = position.copy(
                        x = position.x - newSize2,
                        y = position.y - newSize2
                    ),
                    direction = (direction + random.nextDouble(-angle, angle).toFloat()) * -1,
                    speed = speed * (newSize / size)
                )
            )
        }
        if (newSize2 > roidSizeLimit) {
            toAdd.add(
                copy(
                    size = newSize2,
                    position = position.copy(
                        x = position.x + newSize,
                        y = position.y + newSize
                    ),
                    direction = (direction + random.nextDouble(-angle, angle).toFloat()) * -1,
                    speed = speed * (newSize2 / size)
                )
            )
        }
        return toAdd
    }

    private fun distance(p1: Position, p2: Position): Double {
        return sqrt((p1.x - p2.x).pow(2) + (p1.y - p2.y).pow(2))
    }

    private fun Float.pow(n: Int): Double {
        return toDouble().pow(n)
    }

    private fun Body.checkBounds() {
        if (position.x < 0) position.x = width.toFloat()
        if (position.x > width) position.x = 0f
        if (position.y < 0) position.y = height.toFloat()
        if (position.y > height) position.y = 0f
    }

    private fun playerAlive(): Boolean {
        return bodies.none {
            val distance = distance(it.position, player.position)
            val minDistance = it.size + player.size
            distance <= minDistance
        }
    }
}
