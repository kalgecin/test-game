package com.example.testgame

import com.kalgecin.neataptic4k.NEAT
import com.kalgecin.neataptic4k.Network
import kotlinx.serialization.decodeFromString
import kotlinx.serialization.json.Json
import org.slf4j.Logger
import java.lang.Thread.sleep
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.math.pow

fun loadNeatPopulation(filename: String): Array<Network> {
    val json = Json { }
    val neatPath = Paths.get(filename)
    return if (Files.exists(neatPath)) {
        val savedNeat = Files.readString(neatPath)
        return try {
            val savedPopulation = json
                .decodeFromString<List<Network.CopyObject>>(savedNeat)
                .map { Network.deserialize(it) }
                .toTypedArray()
            println("imported saved neat generations")
            savedPopulation
        } catch (e: Exception) {
            println("Failed to deserialize $filename: ${e.message}")
            sleep(5000)
            emptyArray()
        }
    } else emptyArray()
}

fun NEAT.printStats(logger: Logger) {
    val scores = population.map { it.score }.sortedDescending()
    val max = scores.maxOrNull()
    val min = scores.minOrNull()
    val avg = scores.average()
    val topAvg = scores.take(10).average()
    logger.info("$generation $max $topAvg $avg $min")
}

fun Double.round(sf: Int): Double {
    val pow = (10.0).pow(sf)
    return (this * pow).toInt() / pow
}
