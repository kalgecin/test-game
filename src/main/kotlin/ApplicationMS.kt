import io.micronaut.runtime.Micronaut
import org.slf4j.Logger
import org.slf4j.LoggerFactory

object ApplicationMS {

    @JvmStatic
    fun main(args: Array<String>) {
        Micronaut.build()
            .packages("com.example.testgame")
            .mainClass(ApplicationMS::class.java)
            .start()
    }
}

fun getLogger(forClass: Class<*>): Logger = LoggerFactory.getLogger(forClass)
