import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import com.example.testgame.GameUI
import com.example.testgame.game.AiGame
import com.example.testgame.game.GdxGame
import com.example.testgame.loadNeatPopulation
import com.kalgecin.neataptic4k.EvolveOptions
import com.kalgecin.neataptic4k.MutationType
import com.kalgecin.neataptic4k.NEAT
import com.kalgecin.neataptic4k.Network
import kotlinx.coroutines.*
import kotlinx.serialization.encodeToString
import kotlinx.serialization.json.Json
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.random.Random
import kotlin.system.measureTimeMillis

const val neatFilename = "savedNEAT.json"
//const val neatFilename = "savedNEAT_big.json"
var bestNetwork: Network? = null

object Application {
    @JvmStatic
    fun main(args: Array<String>) {

        val config = LwjglApplicationConfiguration().apply {
            width = 1280
            height = 720
//        vSyncEnabled = false
//        backgroundFPS = 0
//        foregroundFPS = 0
        }

        when (args.getOrNull(0)) {
            "player" -> {
                val g = GdxGame(config.width, config.height)
                LwjglApplication(GameUI(g), config)
            }
            "noui" -> {
                runBlocking { neatCoroutine(config.width, config.height) }
            }
            else -> {
                val game = AiGame(config.width, config.height, bestNetwork)
                LwjglApplication(GameUI(game), config)
//            LwjglApplication(NetworkViewUI(game), config)
                runBlocking {
                    launch {
                        while (true) {
                        game.tick()
                            if (!game.playerAlive) {
//                                val loadNeatPopulation = loadNeatPopulation(neatFilename)
//                                bestNetwork = if (loadNeatPopulation.isNotEmpty()) loadNeatPopulation.getOrNull(
//                                    Random.nextInt(loadNeatPopulation.size)
//                                ) else bestNetwork
                                game.network = bestNetwork
                                game.reset()
                            }
                            delay(5000)
                        }
                    }
                neatCoroutine(config.width, config.height)
                }
            }
        }
    }

}

suspend fun neatCoroutine(width: Int, height: Int) = withContext(Dispatchers.Default) {
    val neat = loadNeat(neatFilename, 500)
    while (true) {
        val time = measureTimeMillis {
            val seed = Random(Random.nextLong())
            neat.population.map { network ->
                async {
                    val game = AiGame(width, height, network, seed)
                    while (game.playerAlive) {
                        game.tick()
//                        delay(16)
                    }
                    network.score = game.score.toDouble()
//                    println(game.score)
                }
            }.forEach { it.await() }
        }
        bestNetwork = neat.population.maxByOrNull { it.score }?.copy()
        val scores = neat.population.map { it.score }.sortedDescending()
        val max = scores.maxOrNull()
        val min = scores.minOrNull()
        val avg = scores.average()
        val topAvg = scores.take(10).average()
        println("${time}ms ${neat.generation} $max $topAvg $avg $min")
        saveAndEvolve(neat)
    }
}

fun saveAndEvolve(neat: NEAT) {
    saveNeat(neat)
    neat.options.network = neat.population.maxBy { it.score } ?: neat.options.network
    neat.evolve()
}

fun saveNeat(neat: NEAT) {
    val json = Json { }
    val neatJson = json.encodeToString(neat.population.map { it.serialize() })
    Files.writeString(Paths.get(neatFilename), neatJson)
}

fun loadNeat(filename: String, poolSize: Int): NEAT {
    val inputSize = 51
    val outputSize = 8
    val hiddenSize = 0
    val savedPopulation = loadNeatPopulation(filename).toMutableList()
    println("Generating network")
    println("network generated")
    val blueprint = if (savedPopulation.isNotEmpty()) savedPopulation.random() else Network(inputSize, outputSize)
    val options = EvolveOptions(
        mutationTypes = MutationType.ALL,
        populationSize = poolSize,
        mutationRate = 0.1,
        mutationAmount = 3,
        elitism = (poolSize * 0.2).toInt(),
        network = blueprint,
        isClear = true
    )
    val neat = NEAT(inputSize, outputSize, null, options)
    if (savedPopulation.isNotEmpty()) {
        println("Loaded saved neat: ${savedPopulation.size}")
        val popDiff = (0 until options.populationSize - savedPopulation.size).map { neat.getOffspring() }
        savedPopulation.addAll(popDiff)
        neat.import(savedPopulation.take(options.populationSize).toTypedArray())
    }
    println("NEAT generated: ${neat.population.size}")
    return neat
}

