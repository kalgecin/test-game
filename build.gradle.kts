import org.gradle.jvm.tasks.Jar
import com.github.jengelman.gradle.plugins.shadow.tasks.ShadowJar

plugins {
    java
    kotlin("jvm") version "1.4.0"
    kotlin("plugin.serialization") version "1.4.0"
    kotlin("kapt") version "1.4.0"
    id("org.jetbrains.kotlin.plugin.allopen") version "1.4.0"
    id("com.github.johnrengelman.shadow") version "4.0.4"
    id("com.google.cloud.tools.jib") version "2.4.0"
}

group = "org.example"
version = "1.0-SNAPSHOT"
jib.from.image = "oracle/graalvm-ce:20.1.0-java11@sha256:21d0cb01226f6b9f1ded61ce694f31c489ed0a81d11e4f599025739ea37e33f3"
jib.to.image = "registry.kalgecin.com/asteroid-game:latest"
jib.container.mainClass = "ApplicationMS"

repositories {
    mavenCentral()
    mavenLocal()
    jcenter()
}

val gdxVersion = "1.9.10"
val micronautVersion: String by project

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core", "1.3.8")
    implementation("com.badlogicgames.gdx", "gdx", gdxVersion)
    implementation("com.badlogicgames.gdx", "gdx-backend-lwjgl", gdxVersion)
    implementation("com.badlogicgames.gdx:gdx-platform:$gdxVersion:natives-desktop")
    implementation("io.github.libktx", "ktx-app", "1.9.10-b2")
    implementation("io.github.libktx", "ktx-graphics", "1.9.10-b2")
    implementation("com.kalgecin", "Neataptic4K-jvm", "1.1.0")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-protobuf:1.0.0-RC")
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-core:1.0.0-RC")
    testImplementation("junit", "junit", "4.12")

    implementation(platform("io.micronaut:micronaut-bom:$micronautVersion"))
    implementation("io.micronaut:micronaut-runtime")
    implementation("io.micronaut.rabbitmq:micronaut-rabbitmq")
    implementation("javax.annotation:javax.annotation-api")
    kapt(platform("io.micronaut:micronaut-bom:$micronautVersion"))
    kapt("io.micronaut:micronaut-inject-java")
    kapt("io.micronaut:micronaut-validation")
    kaptTest(platform("io.micronaut:micronaut-bom:$micronautVersion"))
    kaptTest("io.micronaut:micronaut-inject-java")
    runtimeOnly("com.fasterxml.jackson.module:jackson-module-kotlin:2.9.8")
    runtimeOnly("ch.qos.logback:logback-classic:1.2.3")
    testImplementation(platform("io.micronaut:micronaut-bom:$micronautVersion"))
    testImplementation("io.micronaut.test:micronaut-test-kotlintest")
    testImplementation("io.mockk:mockk:1.9.3")
    testImplementation("io.kotlintest:kotlintest-runner-junit5:3.3.2")
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
tasks {
    compileKotlin {
        kotlinOptions.jvmTarget = "1.8"
        kotlinOptions.javaParameters = true
    }
    compileTestKotlin {
        kotlinOptions.jvmTarget = "1.8"
        kotlinOptions.javaParameters = true
    }

    named<ShadowJar>("shadowJar") {
        archiveBaseName.set("AsteroidGame")
        mergeServiceFiles()
        manifest {
            attributes(mapOf("Main-Class" to "ApplicationKt"))
        }
    }
}

val fatJar = task("fatJar", type = Jar::class) {
    baseName = "${project.name}-fat"
    manifest {
        attributes["Implementation-Title"] = "Gradle Jar File Example"
        attributes["Implementation-Version"] = version
        attributes["Main-Class"] = "ApplicationKt"
    }
    from(configurations.runtimeClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    from(configurations.compileClasspath.get().map { if (it.isDirectory) it else zipTree(it) })
    with(tasks.jar.get() as CopySpec)
}

tasks {
    "build" {
        dependsOn(shadowJar)
    }
}

allOpen {
    annotation("io.micronaut.aop.Around")
}
